﻿using Microsoft.AspNetCore.Mvc;
using MVCEmployes.Core;
using MVCEmployes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Controllers
{
    public class EmployesController : Controller
    {
        IEmployesRepository _empl;
        public EmployesController(IEmployesRepository empl)
        {
            _empl = empl;
        }
        public IActionResult Index()
        {
            var model_list = _empl.GetEmployes();
            return View(model_list);
        }

        public IActionResult DebugFormEmpl()
        {
            return View();
        }
        public IActionResult EmployeeList()
        {
            return ViewComponent("EmployeeList");
        }

        public IActionResult EmplForm(int id)
        {
            return ViewComponent("EmplForm", new { id = id });
        }

        [HttpPost]
        public JsonResult SubmitEmpl(EditFormEmployee empl)
        {

            // если есть лшибки - возвращаем false
            if (!ModelState.IsValid)
                return Json(false);

            try
            {
                Employes model = new Employes
                {
                    id = empl.edit_id,
                    FirstName = empl.edit_FirstName,
                    LastName = empl.edit_LastName
                };

                return Json(_empl.SubmitEmployee(model));
            }
            catch
            {
                return Json(-1);
            }
        }

        [HttpPost]
        public JsonResult DeleteEmpl(int id_empl)
        {
            try
            {
                _empl.RemoveEmployee(id_empl);
                return Json(0);
            }
            catch
            {
                return Json(-1);
            }
        }
    }
}
