﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Models
{
    public class Employes : IGeneralField , IEmplId, IModify
    {
        [Key]
        public int id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime? DateUpdate { get; set; }

    }

    public interface IEmplId
    {
        int id { get; set; }
    }
    //основная форма
    public interface IGeneralField : IEmplId
    {
        string FirstName { get; set; }
        string LastName { get; set; }        
    }

    public interface IModify : IEmplId
    {
        DateTime? DateUpdate { get; set; }
    }
}
