﻿using Microsoft.AspNetCore.Mvc;
using MVCEmployes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Components
{
    public class EmplFormViewComponent : ViewComponent
    {
        IEmployesRepository _empl;

        public EmplFormViewComponent(IEmployesRepository empl)
        {
            _empl = empl;
        }
        public IViewComponentResult Invoke(int  id)
        {
            EditFormEmployee form;
            if (id == 0)
            {
                form = new EditFormEmployee { edit_id = 0 };
            }
            else
            {
                var empl = _empl.GetEmployee(id);

                form = new EditFormEmployee
                {
                    edit_id = empl.id,
                    edit_FirstName = empl.FirstName,
                    edit_LastName = empl.LastName
                };
            }
            return View(form);
        }
    }
}
