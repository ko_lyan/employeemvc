﻿using Microsoft.AspNetCore.Mvc;
using MVCEmployes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Components
{
    public class EmployeeListViewComponent: ViewComponent
    {
        IEmployesRepository _empl;

        public EmployeeListViewComponent(IEmployesRepository empl)
        {
            _empl =empl;
        }
        public IViewComponentResult Invoke()
        {

            return View(_empl.GetEmployes());
        }
    }
}
