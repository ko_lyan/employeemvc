﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes
{
    public class EditFormEmployee
    {
        public int edit_id { get; set; }

        public string edit_FirstName { get; set; }
        public string edit_LastName { get; set; }
    }
}
