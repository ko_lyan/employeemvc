﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Core
{
    public static class Ext
    {
        //Обновлем модель по её интерфейсам. Помогает, если таблица большая, 
        //а мы не хотим обновлять все её поля, чтоб избежать затираний и всякого такого
        //тож перебирает филды через рефлексию, и сравнивает с интерфейсными
        //    если совпадает - делает свойству поля что оно  модифицирвано
        public static void UpdateFromInterface<TEntity, TInterface>(this EmployesContext _Context, TEntity model)
where TEntity : class, TInterface

        {
            var Imodel = typeof(TInterface);

            var props = Imodel.GetProperties()
                .Select(x => x.Name).ToList();
            try
            {

                if (!_Context.Set<TEntity>().Local.Any(e => e == model))
                    _Context.Set<TEntity>().Attach(model);

                foreach (var field in props)
                {

                    _Context.Entry(model).Property(field).IsModified = true;
                }

            }
            catch (Exception ex)
            {

            }
        }

    }
}
