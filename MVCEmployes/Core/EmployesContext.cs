﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MVCEmployes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Core
{
    public class EmployesContext:DbContext
    {
        public DbSet<Employes> Employes { get; set; }
        public EmployesContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");
            IConfiguration AppConfiguration = builder.Build();
            optionsBuilder.UseSqlServer(AppConfiguration.GetConnectionString("DefaultConnection"));

        }
        //Если модель реализует IModify - то задаёт дату редактирования
        public override int SaveChanges()
        {
            var i_modify = ChangeTracker.Entries<IModify>();

            if (i_modify != null)
            {
                DateTime this_time = DateTime.Now;
                foreach (var entry in i_modify.Where(c => c.State == EntityState.Modified || c.State == EntityState.Added || c.State == EntityState.Deleted ))
                {
                        entry.Entity.DateUpdate = this_time;

                }
            }

            return base.SaveChanges();
        }
    }
}
