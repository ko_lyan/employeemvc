﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MVCEmployes.Core
{
    //класс с настройками зависимостей
    public abstract class AbstractBaseSettings
    {
        public EmployesContext _Context;
        public AbstractBaseSettings()
        {
            _Context = new EmployesContext();
        }

        //Сравнивает модели по их интерфейсам через рефлексию
        public bool CompareTwoClassByInterface<TInterface, TEntity>(TEntity model1, TEntity model2) where TEntity : class, TInterface
        {

            var Imodel = typeof(TInterface);

            var props = Imodel.GetProperties()
                .Select(x => x.Name).ToList();


            foreach (var field in props)
            {

                PropertyInfo fld = typeof(TEntity).GetProperty(field);
                object val1 = fld.GetValue(model1);
                object val2 = fld.GetValue(model2);

                if (val1 == null && val2 == null)
                    continue;

                if ((val1 == null && val2 != null) || (val1 != null && val2 == null))
                    return false;

                if (!val1.Equals(val2))
                    return false;
            }

            return true;
        }

    }
}
