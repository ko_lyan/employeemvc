﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MVCEmployes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCEmployes.Core
{
    public class EmployesRepository : AbstractBaseSettings, IEmployesRepository
    {
 
        //Получение Эмплоя
        public List<Employes> GetEmployes()
        {

            return _Context.Employes.ToList();
        }

        public Employes GetEmployee(int id)
        {
            return _Context.Employes.Where(x => x.id == id).FirstOrDefault();
        }
        //Сохранение 
        public int SubmitEmployee(Employes model)
        {
            if(model.id==0)
            {
                //_Context.Entry(model).Property("DateUpdate").Va

                _Context.Employes.Add(model);
                
                _Context.SaveChanges();
                return model.id;
            }
            else
            {
                bool formUpdate = false;
                //забираем модель из базы, и делаем неотслеживаемой, чтоб не мешалась
                var change_detalies = _Context.Employes.Where(x => x.id == model.id).FirstOrDefault();
                _Context.Entry(change_detalies).State = EntityState.Detached;
                //сравниваем изменяемые поля
                formUpdate = CompareTwoClassByInterface<IGeneralField, Employes>(model, change_detalies);

                //если поля изменились обновляем
                if (!formUpdate)
                    _Context.UpdateFromInterface<Employes, IGeneralField>(model);
                else
                    throw new Exception("Значение уже задано!!!");
                    //return model.id;//

                _Context.SaveChanges();

                return model.id;
            }
        }
        //удаление
        public void RemoveEmployee(int id)
        {
            var delete = _Context.Employes.Where(x => x.id == id).FirstOrDefault();
            if (delete == null)
                throw new Exception("Сотрудник не найден");

            _Context.Remove(delete);
            _Context.SaveChanges();
        }
    }
    public interface IEmployesRepository
    {
        List<Employes> GetEmployes();
        Employes GetEmployee(int id);
        int SubmitEmployee(Employes model);
        void RemoveEmployee(int id);
    }
}
