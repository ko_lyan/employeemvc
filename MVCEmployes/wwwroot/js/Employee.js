﻿(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


function submitEmpl() {

    var user_form_s = $("#formEmpl").serialize();

    if ($("#formEmpl")[0].checkValidity()) {
        event.preventDefault();
    var link = '/Employes/SubmitEmpl'
    $.ajax({
        url: link,
        type: 'POST',
        data: user_form_s,
        success: function (data) {
            $('#myModalEmplForm').modal('hide');
            PerformCallbackTable();
        },
        error: function () {
            console.log('Ошибка во время отправки комментария', this);
        }
    });

  }
}

function delete_empl (id_empl)
{
    var link = '/Employes/DeleteEmpl'
    $.ajax({
        url: link,
        type: 'POST',
        data: {
            id_empl: id_empl
        },
        success: function (data) {
            PerformCallbackTable();
        },
        error: function () {
            console.log('Ошибка во время отправки комментария', this);
        }
    });
}
function openDetalies(empl_id) {
 
    $('#EmployeeFormDiv').html('');
    $('#myModalEmplForm').modal();
    //window.setInterval(refreshComponent, 1000);
    $.get("/Employes/EmplForm", { id: empl_id }, function (data) { $('#EmployeeFormDiv').html(data); });

}


function PerformCallbackTable() {
    //поиск 

    //каллбэк таблицы
    $.get("/Employes/EmployeeList", function (data) { $('#table_empls').html(data); });

}