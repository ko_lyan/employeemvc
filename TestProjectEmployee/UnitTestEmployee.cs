using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVCEmployes.Core;
using MVCEmployes.Models;
using System;
using System.Linq;

namespace TestProjectEmployee
{
    [TestClass]
    public class UnitTestEmployee
    {
        IEmployesRepository _empl;

        //Mock<IEmployesRepository> _moqEmpl;
        [TestInitialize]
        public void Setup()
        {
            _empl = new EmployesRepository();

        }

        //��������� ������ 
        [TestMethod]
        public void TestListEmployee()
        {
            var lst = _empl.GetEmployes();

            CollectionAssert.AllItemsAreNotNull(lst);
        }
        //���������� ����������
        [TestMethod]
        public void TestAddEmploye()
        {
            Employes add = new Employes
            {
                id=0,
                FirstName = Guid.NewGuid().ToString(),
                LastName = Guid.NewGuid().ToString()
            };
            _empl.SubmitEmployee(add);
        }

        //���������� ����������
        [TestMethod]
        public void TestUpdEmploye()
        {
            Employes upd = new Employes
            {
                id = 2,
                FirstName = Guid.NewGuid().ToString(),
                LastName = Guid.NewGuid().ToString()
            };
            _empl.SubmitEmployee(upd);
        }
        //�������� ���������� ���� �������
        [TestMethod]
        public void TestDeleteEmployee()
        {
            var emp = _empl.GetEmployes().LastOrDefault();
            if(emp!= null)
                _empl.RemoveEmployee(emp.id);
        }

        [TestMethod]
        public void GetEmployee()
        {

        }
    }
}
